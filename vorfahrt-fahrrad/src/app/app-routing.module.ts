import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddTrafficLightMapComponent } from './components/add-traffic-light-map/add-traffic-light-map.component';
import { IndexPageComponent } from './components/index-page/index-page.component';
import { AddTrafficLightDetailsComponent } from './components/add-traffic-light-details/add-traffic-light-details.component';
import { CityOverviewComponent } from './components/city-overview/city-overview.component';

const routes: Routes = [
  { path: '', component: IndexPageComponent },
  {
    path: 'addTrafficLightMap',
    component: AddTrafficLightMapComponent,
  },
  {
    path: 'addTrafficLightDetails',
    component: AddTrafficLightDetailsComponent,
  },
  {
    path: 'cityOverview',
    component: CityOverviewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
