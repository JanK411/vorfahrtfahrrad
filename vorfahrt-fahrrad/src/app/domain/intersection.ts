export interface Intersection {
  name: string;
  osm_id: string;
  created_at: Date;
  lat: string;
  lon: string;
}
