import { Component, Input, OnInit } from '@angular/core';
import { IntersectionService } from '../../../services/intersection.service';

@Component({
  selector: 'intersection[name][osm_id]',
  templateUrl: './intersection.component.html',
  styleUrls: ['./intersection.component.scss'],
})
export class IntersectionComponent implements OnInit {
  @Input()
  public name!: string;
  @Input()
  public osm_id!: string;
  public images?: string[];

  constructor(private intersectionService: IntersectionService) {}

  async ngOnInit(): Promise<void> {
    this.images = await this.intersectionService.retrieveImagesForIntersection(
      this.osm_id
    );
    console.log(this.images);
  }
}
