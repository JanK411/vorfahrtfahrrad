import { Component, OnInit } from '@angular/core';
import { IntersectionService } from '../../services/intersection.service';
import { Intersection } from '../../domain/intersection';

@Component({
  selector: 'city-overview',
  templateUrl: './city-overview.component.html',
  styleUrls: ['./city-overview.component.scss'],
})
export class CityOverviewComponent implements OnInit {
  public intersections!: Intersection[];

  constructor(private intersectionService: IntersectionService) {}

  async ngOnInit(): Promise<void> {
    this.intersections = await this.intersectionService.getAllIntersections();
  }
}
