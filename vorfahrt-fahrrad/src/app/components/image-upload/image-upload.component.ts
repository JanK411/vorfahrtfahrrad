import { Component, Input } from '@angular/core';
import { IntersectionService } from '../../services/intersection.service';

@Component({
  selector: 'image-upload[osm_id]',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
})
export class ImageUploadComponent {
  @Input()
  public osm_id!: string;

  private selectedFile!: File;

  constructor(private intersectionService: IntersectionService) {}

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  async onSubmit() {
    if (!this.selectedFile) {
      return;
    }

    const blob = await this.convertFileToBlob(this.selectedFile);
    const { data, error } =
      await this.intersectionService.uploadImageForIntersection(
        this.osm_id,
        blob
      );

    if (error) {
      console.error(error);
    } else {
      console.log(data);
    }
  }

  private async convertFileToBlob(file: File): Promise<Blob> {
    return new Promise<Blob>((resolve) => {
      const reader = new FileReader();
      reader.onload = () => {
        const byteString = reader.result as string;
        const binary = atob(byteString.split(',')[1]);
        const array = [];
        for (let i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        resolve(new Blob([new Uint8Array(array)], { type: file.type }));
      };
      reader.readAsDataURL(file);
    });
  }
}
