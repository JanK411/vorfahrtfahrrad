import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss'],
})
export class IndexPageComponent {
  constructor(private router: Router) {}

  public async addProblematicTrafficLightSituation() {
    await this.router.navigate(['/addTrafficLightMap']);
  }

  public async navigateToOverviewOfCity() {
    await this.router.navigate(['/cityOverview']);
  }
}
