import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrafficLightDetailsComponent } from './add-traffic-light-details.component';

describe('AddTrafficLightDetailsComponent', () => {
  let component: AddTrafficLightDetailsComponent;
  let fixture: ComponentFixture<AddTrafficLightDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTrafficLightDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddTrafficLightDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
