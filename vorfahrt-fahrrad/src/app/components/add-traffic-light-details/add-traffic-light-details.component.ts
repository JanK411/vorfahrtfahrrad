import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeoLocationNamingService } from '../../services/geo-location-naming.service';
import { firstValueFrom } from 'rxjs';
import { IntersectionService } from '../../services/intersection.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-traffic-light-details',
  templateUrl: './add-traffic-light-details.component.html',
  styleUrls: ['./add-traffic-light-details.component.scss'],
})
export class AddTrafficLightDetailsComponent implements OnInit {
  public lat!: number;
  public lon!: number;
  public name!: string;
  public osm_id!: string;

  constructor(
    private route: ActivatedRoute,
    private locationNamingService: GeoLocationNamingService,
    private intersectionService: IntersectionService,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit(): Promise<void> {
    const params = await firstValueFrom(this.route.queryParams);
    this.lat = params['lat'];
    this.lon = params['lon'];
    if (!this.lat || !this.lon) {
      const msg = 'no lat or lon provided';
      alert(msg);
      throw new Error(msg);
    }
    this.locationNamingService
      .retrieveIntersectionData(this.lat, this.lon)
      .subscribe({
        next: (it) => {
          this.name = it.name;
          this.osm_id = it.osm_id;
        },
        error: (err) => {
          const msg = 'Error retrieving name: ' + err;
          this.name = msg;
          throw new Error(msg);
        },
      });
  }

  allDataPresent(): boolean {
    return !!this.name;
  }

  async submit() {
    console.log('submitting traffic light:', this.name, this.lat, this.lon);
    await this.intersectionService.createIntersection(
      this.name,
      this.osm_id,
      this.lat,
      this.lon
    );
    this.snackBar.open('Dankeschön!');
  }
}
