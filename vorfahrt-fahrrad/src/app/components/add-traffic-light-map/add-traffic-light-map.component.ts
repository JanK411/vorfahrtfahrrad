import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { LatLng } from 'leaflet';
import { GeoLocationNamingService } from '../../services/geo-location-naming.service';
import { Router } from '@angular/router';

const ZOOM: number = 20;

@Component({
  selector: 'app-add-traffic-light-map',
  templateUrl: './add-traffic-light-map.component.html',
  styleUrls: ['./add-traffic-light-map.component.scss'],
})
export class AddTrafficLightMapComponent implements OnInit {
  /** Karte, die in der Komponente angezeigt wird. */
  private map!: L.Map;
  /** Marker, der auf der Karte gesetzt ist. */
  private marker: L.Marker;

  constructor(
    private geoNameService: GeoLocationNamingService,
    private router: Router
  ) {
    L.Icon.Default.prototype.options.shadowUrl =
      'assets/images/marker-shadow.png';
    this.marker = L.marker(new LatLng(42, 42));
  }

  ngOnInit(): void {
    this.map = L.map('map').setView([51.505, -0.09], ZOOM);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
      maxZoom: 18,
      tileSize: 512,
      zoomOffset: -1,
    }).addTo(this.map);

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const latlng = L.latLng(
          position.coords.latitude,
          position.coords.longitude
        );
        this.marker = L.marker(latlng);
        this.marker.addTo(this.map);
        this.map.setView(latlng, ZOOM);
      });
    }

    this.map.on('click', (e: L.LeafletMouseEvent) => {
      this.marker.setLatLng(e.latlng);
    });
  }

  async addTrafficLightLocation() {
    console.log(
      await this.router.navigate(['addTrafficLightDetails'], {
        queryParams: {
          lat: this.marker.getLatLng().lat,
          lon: this.marker.getLatLng().lng,
        },
      })
    );
    // this.geoNameService
    //   .getNameForLatLon(
    //     this.marker.getLatLng().lat,
    //     this.marker.getLatLng().lng
    //   )
    //   .subscribe((it) => console.log('traffic light location name: ' + it));
  }
}
