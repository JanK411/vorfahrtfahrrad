import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrafficLightMapComponent } from './add-traffic-light-map.component';

describe('AddTrafficLightComponent', () => {
  let component: AddTrafficLightMapComponent;
  let fixture: ComponentFixture<AddTrafficLightMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddTrafficLightMapComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AddTrafficLightMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
