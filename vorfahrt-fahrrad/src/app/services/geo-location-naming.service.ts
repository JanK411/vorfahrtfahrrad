import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';

interface NominatimResponse {
  osm_id: string;
  lat: number;
  lon: number;
  category: string;
  name: string;
  address: {
    road: string;
    suburb: string;
    city_district: string;
    city: string;
    county: string;
    state: string;
    postcode: string;
    country: string;
    house_number: string;
    place: string;
  };
}

const ZOOM_LEVEL = 18;

@Injectable({
  providedIn: 'root',
})
export class GeoLocationNamingService {
  private readonly url =
    'https://nominatim.openstreetmap.org/reverse?format=jsonv2&zoom=' +
    ZOOM_LEVEL +
    '&namedetails=1';

  constructor(private http: HttpClient) {}

  public retrieveNominatimDataForLatLon(
    lat: number,
    lon: number
  ): Observable<NominatimResponse> {
    return this.http.get<NominatimResponse>(
      `${this.url}&lat=${lat}&lon=${lon}`
    );
  }

  public retrieveIntersectionData(
    lat: number,
    lon: number
  ): Observable<{ name: string; osm_id: string }> {
    return this.retrieveNominatimDataForLatLon(lat, lon).pipe(
      map((data) => {
        const houseNumber = data.address.house_number;
        return {
          name: `${data.address.road}${houseNumber ? ' ' + houseNumber : ''}, ${
            data.address.suburb
          }, ${data.address.postcode} ${data.address.city}`,
          osm_id: data.osm_id,
        };
      })
    );
  }
}
