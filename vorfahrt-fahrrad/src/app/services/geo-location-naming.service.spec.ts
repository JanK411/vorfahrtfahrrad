import { TestBed } from '@angular/core/testing';

import { GeoLocationNamingService } from './geo-location-naming.service';

describe('GeoLocationNamingService', () => {
  let service: GeoLocationNamingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeoLocationNamingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
