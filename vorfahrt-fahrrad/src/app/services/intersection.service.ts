import { Injectable } from '@angular/core';
import { supabase } from '../util/supabase.constants';
import { Intersection } from '../domain/intersection';

const INTERSECTION_IMAGES_BUCKET = 'intersection_images';

@Injectable({
  providedIn: 'root',
})
export class IntersectionService {
  constructor() {}

  async createIntersection(
    name: string,
    osm_id: string,
    lat: number,
    lon: number
  ) {
    const { error } = await supabase.from('intersections').insert([
      {
        osm_id,
        created_at: new Date(),
        name,
        lat,
        lon,
      },
    ]);
    if (error) {
      console.log(error);
      alert(error.message);
    }
  }

  async getAllIntersections() {
    let { data, error } = await supabase.from('intersections').select('*');
    const intersections: Intersection[] | undefined = data?.map((it) => {
      return {
        name: it['name'],
        osm_id: it['osm_id'],
        lat: it['lat'],
        lon: it['lon'],
        created_at: it['created_at'],
      };
    });
    if (!intersections) {
      throw new Error(`no intersections found: ${error?.message}`);
    }
    return intersections;
  }

  public async uploadImageForIntersection(osmId: string, blob: Blob) {
    const { data, error } = await supabase.storage
      .from(INTERSECTION_IMAGES_BUCKET)
      .upload(`${osmId}/${new Date().valueOf()}`, blob);
    return { data, error };
  }

  async retrieveImagesForIntersection(osm_id: string): Promise<string[]> {
    const { data: fileListData, error } = await supabase.storage
      .from(INTERSECTION_IMAGES_BUCKET)
      .list(osm_id);
    if (error) {
      console.error(error);
      throw new Error(error.message);
    }

    // Filter the list to only include files with image MIME types
    return fileListData
      .filter((file) => file.metadata['mimetype'].startsWith('image/'))
      .map((fileName) => {
        return supabase.storage
          .from(INTERSECTION_IMAGES_BUCKET)
          .getPublicUrl(osm_id + '/' + fileName.name).data.publicUrl;
      });
  }
}
